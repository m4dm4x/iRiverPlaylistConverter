# iriver E100 playlist converter

This is a simple m3u (file list) to iriver playlist converter.
The program reads the standard input stream and writes to the standard output stream.

Convert from m3u to pla:

	m3u2pla < playlist.m3u > playlist.pla

To convert from pla to m3u use the parameter `x` (eXtract):

	m3u2pla x < playlist.pla > playlist.m3u

Any path manipulation or directory separator replacement has to be done before the playlist
is send to the program.

## Create a playlist

Simple playlist on the fly:

	find Album/ -type f -print | sed -e 's/^/\\Music\\/' -e 's/[/]/\\/g' | m3u2pla > Album.pla

Replace path prefix:

	find /media/E100/Music/Album -type f -print | \
	sed -e 's/^[/]media[/]E100//' -e 's/[/]/\\/g' | sort | \
	m3u2pla >/media/E100/Playlists/Album.pla

Playlist creator script:

	makepla() {
		find "$1" -type f -print | sed -e 's/^/\\Music\\/' -e 's/[/]/\\/g' | sort | m3u2pla > "${1%/}.pla"
	}

## Resources

* <https://en.wikipedia.org/wiki/Iriver_E100>
* <http://phintsan.kapsi.fi/iriver-t50.html>
* <https://wiki.bath.ac.uk/display/~ma9mjo/iRiver+s10>
