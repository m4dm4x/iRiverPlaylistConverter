cmake_minimum_required(VERSION 3.6)
project(PlaylistConverter CXX)

set(CMAKE_CXX_STANDARD 14)

# Create the m3u2pla executable
add_executable(m3u2pla src/main.cxx src/pla.cxx src/txt.cxx)
if (WIN32)
	target_link_libraries(m3u2pla Ws2_32)
endif()

# Install the executable
install(TARGETS m3u2pla DESTINATION bin)
