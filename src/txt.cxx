#include "txt.hxx"

#include <iostream>
#include <iterator>

namespace txt {

static constexpr auto FirstFileNameChar = '\\';

static constexpr auto MaxFileNameSize = (pla::BlockSize / 2) - 2;

std::istream& read(std::istream &in, pla::FileList &fileList) {
	std::string fileName;
	while (std::getline(in, fileName)) {
		if (fileName.empty()) {
			continue;
		} else if (FirstFileNameChar != fileName[0]) {
			throw std::runtime_error{"expected first char '\\' in: " + fileName};
		} else if (MaxFileNameSize < fileName.size()) {
			throw std::length_error{"too long: " + fileName};
		}
		fileList.push_back(fileName);
	}
	return in;
}

std::ostream& write(std::ostream &out, const pla::FileList &fileList) {
	std::copy(fileList.cbegin(), fileList.cend(), std::ostream_iterator<std::string>{out, "\n"});
	return out;
}

};
