#include <iostream>

#include "pla.hxx"
#include "txt.hxx"

int main(int argc, char *argv[]) {
	std::istream &in = std::cin;
	std::ostream &out = std::cout;
	pla::FileList fileList;
	try {
	if (argc > 1 && 'x' == argv[1][0]) {
			pla::read(in, fileList);
			txt::write(out, fileList);
	} else {
			txt::read(in, fileList);
			pla::write(out, fileList);
	}
	} catch (const std::exception &e) {
		std::cerr << "error: " << e.what() << std::endl;
	}
}

