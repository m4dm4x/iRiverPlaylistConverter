#include "pla.hxx"

#include <iostream>
#include <locale>
#include <codecvt>
#include <algorithm>
#include <iterator>

#ifdef __unix__
#include <netinet/in.h>
using U16CharType = char16_t;
#endif // __unix__

#ifdef _WIN32
#include <WinSock2.h>

#ifdef __MINGW32__
using U16CharType = char16_t;
#endif // __MINGW32__

#ifdef _MSC_VER
using U16CharType = __int16;
#endif // _MSC_VER

#endif // _WIN32

namespace { // <anonymous>

template<typename T>
static inline std::istream& readData(std::istream &in, T &data) {
	return in.read(reinterpret_cast<char*>(&data), sizeof(T));
}

/// write raw data to output stream
template<typename T>
static inline std::ostream& writeData(std::ostream &out, const T &data) {
	return out.write(reinterpret_cast<const char*>(&data), sizeof(T));
}

/// fill output stream with padding null bytes
static inline std::ostream& writePadding(std::ostream &out, size_t count) {
	std::fill_n(std::ostream_iterator<char>(out), count, char(0));
	return out;
}

// UTF-8 / UTF-16 standard conversion facet
using StringConvert = std::wstring_convert<std::codecvt_utf8_utf16<U16CharType>, U16CharType>;

} // namespace <anonymous>

namespace pla {

std::ostream& write(std::ostream& out, const FileList &fileList) {
//	static_assert(sizeof(uint32_t) == sizeof(htonl(0)));
//	static_assert(sizeof(uint16_t) == sizeof(htons(0)));

	{ // write pla header
		writeData(out, htonl(fileList.size()));
		out.write("iriver UMS PLA", 14);
		writePadding(out, BlockSize - 4 - 14);
	}

	// write file blocks
	for (const auto &fileName : fileList) {
		size_t cnt{2};
		const auto fileName16 = StringConvert{}.from_bytes(fileName);
		const auto fileNameIndex = uint16_t(2 + fileName16.find_last_of('\\'));
		writeData(out, htons(fileNameIndex));
		for (auto ch : fileName16) {
			writeData(out, htons(ch));
			cnt += 2;
		}
		if (BlockSize < cnt) {
			throw std::length_error{"filename block too long"};
		}
		writePadding(out, BlockSize - cnt);
	}
	
	return out;
}

std::istream& read(std::istream& in, FileList &fileList) {
	uint32_t fileCount{0};
	{ // write pla header
		readData(in, fileCount);
		fileCount = ntohl(fileCount);
		in.ignore(512 - 4);
	}

	for (uint32_t i = 0; i < fileCount; ++i) {
		uint16_t fileIndex{0};
		readData(in, fileIndex);
		fileIndex = ntohs(fileIndex);
		if (0 == fileIndex) {
			throw std::runtime_error{"invalid filename index at " + std::to_string(i)};
		}

		size_t cnt{2 + 2};
		U16CharType ch{0};
		StringConvert::wide_string fileName16;
		while (readData(in, ch) && ch) {
			ch = ntohs(ch);
			fileName16.push_back(ch);
			cnt += 2;
		}

		if (fileName16.empty()) {
			throw std::runtime_error{"empty filename at " + std::to_string(i)};
		}

		in.ignore(512 - cnt);

		fileList.push_back(StringConvert{}.to_bytes(fileName16));
	}

	return in;
}

}
