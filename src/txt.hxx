#ifndef TXT_LIB_HXX
#define TXT_LIB_HXX

#include "pla.hxx"

namespace txt {

std::istream& read(std::istream &in, pla::FileList &fileList);

std::ostream& write(std::ostream &out, const pla::FileList &fileList);

};

#endif // TXT_LIB_HXX
