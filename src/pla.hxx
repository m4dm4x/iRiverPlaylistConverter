#ifndef PLA_LIB_HXX
#define PLA_LIB_HXX

#include <iosfwd>
#include <string>
#include <vector>

namespace pla {

/// PLA blocksize in bytes
static constexpr size_t BlockSize = 512;

using FileList = std::vector<std::string>;

std::ostream& write(std::ostream& out, const FileList &fileList);

std::istream& read(std::istream& in, FileList &fileList);

}

#endif
